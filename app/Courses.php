<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use libphonenumber\PhoneNumber;
use libphonenumber\PhoneNumberFormat;


class Courses extends Model
{
    protected $hidden = ['deleted_at','created_at','updated_at'];
    protected $with = ['students'];
    protected $fillable = ['coach_id','nutritionist_id','title','address','start_date','details'];


    public function students()
    {
        return $this->hasMany(CourseStudent::class,'course_id');
    }

    public function coaches()
    {
        return $this->belongsTo(Coaches::class);
    }

}
